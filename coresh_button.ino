// Enable debug prints to serial monitor
#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_RFM69

#define MY_RFM69_FREQUENCY   RF69_433MHZ
//#define MY_RFM69_FREQUENCY   RF69_868MHZ
//#define MY_RFM69_FREQUENCY   RF69_315MHZ
//#define MY_RFM69_FREQUENCY   RF69_915MHZ

#define MY_IS_RFM69HW

#define MY_NODE_ID 0xCA

#include <SPI.h>
#include <MySensors.h>

#define SKETCH_NAME "4 switches node"
#define SKETCH_MAJOR_VER "1"
#define SKETCH_MINOR_VER "0"
 
// Initialising array holding button child ID's
// NULL values used to indicate no switch attached to a connecctor and no child ID need to be added in Controller
int SWITCH_CHILD_ID[5] = {NULL,NULL,1,NULL,NULL};

#define BUTTONS_INTERUPT_PIN 3

// Initialising array holding Arduino Digital I/O pins for button/reed switches
int SWITCH_BUTTON_PIN[5] = {NULL,4,5,6,7};

int BATTERY_SENSE_PIN = A6;  // select the input pin for the battery sense point
int oldBatteryPcnt = 0;
 
// Initialising array holding button state messages
MyMessage msg_switch[5];

void setup() 
{ 
  // Setup the buttons
  // There is no need to activate internal pull-ups
  for (int i = 1; i <= 4; i++){
    pinMode(SWITCH_BUTTON_PIN[i], INPUT);
  }
}

void presentation() {
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo(SKETCH_NAME, SKETCH_MAJOR_VER "." SKETCH_MINOR_VER);
  analogReference(INTERNAL); 
  
  // Register binary input sensor to sensor_node (they will be created as child devices)
  // You can use S_DOOR, S_MOTION or S_LIGHT here depending on your usage.
  // If S_LIGHT is used, remember to update variable type you send in. See "msg" above.
  for (int i = 1; i <= 4; i++){
    if (SWITCH_CHILD_ID[i] !=NULL) {
      msg_switch[i] = MyMessage(SWITCH_CHILD_ID[i], V_LIGHT);
      present(SWITCH_CHILD_ID[i], S_LIGHT);
      }
  }
}
 
// Loop will iterate on changes on the BUTTON_PINs
void loop()
{
  // Button state value arrays
  static uint8_t  value[5] = {NULL, NULL, NULL, NULL, NULL};
  static  uint8_t last_value[5] = {NULL, NULL, NULL, NULL, NULL}; 

  //------------------------------------------------------- get the battery Voltage
   int sensorValue = analogRead(BATTERY_SENSE_PIN);
/*   
   #ifdef MY_DEBUG
   Serial.println(sensorValue);
   #endif
*/   
   // 1M, 470K divider across battery and using internal ADC ref of 1.1V
   // Sense point is bypassed with 0.1 uF cap to reduce noise at that point
   // ((1e6+470e3)/470e3)*1.1 = Vmax = 3.44 Volts
   // 3.44/1023 = Volts per bit = 0.003363075
   
   int batteryPcnt = sensorValue / 10;
/*
   #ifdef MY_DEBUG
   float batteryV  = sensorValue * 0.00312805;
   Serial.print("Battery Voltage: ");
   Serial.print(batteryV);
   Serial.println(" V");

   Serial.print("Battery percent: ");
   Serial.print(batteryPcnt);
   Serial.println(" %");
   #endif
*/
   if (oldBatteryPcnt != batteryPcnt) {
     // Power up radio after sleep
     sendBatteryLevel(batteryPcnt);
     oldBatteryPcnt = batteryPcnt;
   }
  //   Check active switches 
  for (int i = 1; i <= 4; i++){
    if (SWITCH_CHILD_ID[i] != NULL) {
      value[i] = digitalRead(SWITCH_BUTTON_PIN[i]);
      if (last_value[i] != value[i]){
        last_value[i] = value[i];
        Serial.println("VALUE");
        Serial.println(value[i]);
        send(msg_switch[i].set(value[i] == 1 ? false:true), true);
        }
    }
  }
  
  wait(10);
  /*
  if (!isTransportOK()) {
    // TNR: transport not ready
    //transportSwitchSM(stInit);
    //stInitTransition();
    Serial.println("*******TNR: interception*********");
    _begin(); 
  }
  */
  sleep(BUTTONS_INTERUPT_PIN - 2, RISING, 0);
} 
